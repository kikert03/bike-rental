
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registration</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('lte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('lte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('lte/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="" class="h1"><b>Bike</b>RENTAL</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Register a new membership</p>

      
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Full name" id="name" name="name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="email" id="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Retype password" id="re-password" name="re-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="button" class="btn btn-primary btn-block" onclick="return Signup()">Register</button>
          </div>
          <!-- /.col -->
        </div>
     

      <div class="social-auth-links text-center">
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i>
          Sign up using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i>
          Sign up using Google+
        </a>
      </div>

      <a href="login.html" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="{{asset('lte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('lte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('lte/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script>
    const  Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 3000
    }); 
  function Signup()
    {
      let email = $('#email').val()
      let password = $('#password').val()
      let re_password = $('#re-password').val()
      let name = $('#name').val();
      if(name ==='')
        {
          Toast.fire({
            icon: 'error',
            title: ' <span class="text-danger ml-3">Name is required*</span>'
          })
            return false
        }
        if(email ==='')
        {
          Toast.fire({
            icon: 'error',
            title: ' <span class="text-danger ml-3">Email is required*</span>'
          })
            return false
        }
        if(password ==='')
        {
          Toast.fire({
            icon: 'error',
            title: ' <span class="text-danger ml-3">Password is required*</span>'
          })
            return false
        }
        if(re_password === '')
        {
          Toast.fire({
            icon: 'error',
            title: ' <span class="text-danger ml-3">Retype password is required*</span>'
          })
            return false
        }
        if(re_password !== password)
        {
          Toast.fire({
            icon: 'error',
            title: ' <span class="text-danger ml-3">Password does not match*</span>'
          })
            return false
        }
       
      const Register = async() =>
      {
        let data = new FormData()
        data.append('name',name)
        data.append('email',email)
        data.append('password',password)
        
        await axios.post('api/register',data).then(response=>{
         if(response.status ===201)
         {
           window.location.href= "login"
         }
        })
      }
     Register()
    }

    
</script>
</body>
</html>
